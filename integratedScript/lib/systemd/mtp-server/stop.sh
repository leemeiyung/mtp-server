#!/bin/sh
PID=$(cat /var/run/mtp-server-service.pid)
ExsitMtpPid=`ps -d | grep "mtp-server" | awk '{print $1}'`
if [ "$PID" != "undef" ] && [ "$PID" = "$ExsitMtpPid" ]; then
    echo "*** Will kill the old mtp-server PID:${PID} ***"
    kill -9 $PID
    echo "undef" > /var/run/mtp-server-service.pid
elif [ "$ExsitMtpPid" != "" ]; then
    echo "*** Exsit mtp-server PID:${ExsitMtpPid}, kill it ***"
    kill -9 "$ExsitMtpPid"
fi

PID=$(cat /var/run/fswatch.pid)
ExsitFsPid=`ps -d | grep "fswatch" | awk '{print $1}'`
if [ "$PID" != "undef" ] && [ "$PID" = "$ExsitFsPid" ]; then
    echo "*** Will kill the old fswatch PID:${PID} ***"
    kill -9 $PID
    echo "undef" > /var/run/fswatch.pid
elif [ "$ExsitFsPid" != "" ]; then
    echo "*** Exsit fswatch PID:${ExsitFsPid}, kill it ***"
    kill -9 "$ExsitFsPid"
fi

sleep 1
echo "" > /sys/kernel/config/usb_gadget/g1/UDC

exit 0
