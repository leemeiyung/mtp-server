#!/bin/sh

if [ ! -d /sys/kernel/config/usb_gadget/g1 ]; then
    echo "*** usb gadget not configed, will enable mtp-gadget ***"
    mkdir /sys/kernel/config/usb_gadget/g1
    #Set default Vendor and Product IDs for now
    echo 0x18d1 > /sys/kernel/config/usb_gadget/g1/idVendor
    echo 0x4E26 > /sys/kernel/config/usb_gadget/g1/idProduct
    #Create English strings and add random deviceID
    mkdir /sys/kernel/config/usb_gadget/g1/strings/0x409
    echo 202101211140 > /sys/kernel/config/usb_gadget/g1/strings/0x409/serialnumber
    #Update following if you want to
    echo "Vanxoak" > /sys/kernel/config/usb_gadget/g1/strings/0x409/manufacturer
    echo "imx8mm" > /sys/kernel/config/usb_gadget/g1/strings/0x409/product
    #Create gadget configuration
    mkdir /sys/kernel/config/usb_gadget/g1/configs/c.1
    mkdir /sys/kernel/config/usb_gadget/g1/configs/c.1/strings/0x409
    echo "mtp" > /sys/kernel/config/usb_gadget/g1/configs/c.1/strings/0x409/configuration
    echo 120 > /sys/kernel/config/usb_gadget/g1/configs/c.1/MaxPower

    #Create MTP function,
    #And link it to the gadget configuration
    mkdir -p /sys/kernel/config/usb_gadget/g1/functions/mtp.gs0
    ln -s /sys/kernel/config/usb_gadget/g1/functions/mtp.gs0 /sys/kernel/config/usb_gadget/g1/configs/c.1/mtp.gs0
fi
PID=$(cat /var/run/mtp-server-service.pid)
ExsitMtpPid=`ps -d | grep "mtp-server" | awk '{print $1}'`
if [ "$PID" != "" ] && [ "$PID" != "undef" ] && [ "$PID" = "$ExsitMtpPid" ]; then
    echo "*** Will kill the old mtp-server PID:${PID} ***"
    kill -9 $PID
    echo "undef" > /var/run/mtp-server-service.pid
elif [ "$ExsitMtpPid" != "" ]; then
    echo "*** Exsit mtp-server PID:${ExsitMtpPid}, kill it ***"
    kill -9 "$ExsitMtpPid"
fi
/usr/local/bin/mtp-server &
if [ $? = 0 ]; then
    PID=$!
    echo "*** create new mtp-server PID:${PID}***"
    echo ${PID} > /var/run/mtp-server-service.pid
else
    echo "*** create new mtp-server failed ***"
    echo "undef" > /var/run/mtp-server-service.pid
fi
udccfg=`cat /sys/kernel/config/usb_gadget/g1/UDC`
if [ "$udccfg" != "ci_hdrc.0" ]; then
    echo "ci_hdrc.0" > /sys/kernel/config/usb_gadget/g1/UDC
fi

PID=$(cat /var/run/fswatch.pid)
ExsitFsPid=`ps -d | grep "fswatch" | awk '{print $1}'`
if [ "$PID" != "" ] && [ "$PID" != "undef" ] && [ "$PID" = "$ExsitFsPid" ]; then
    echo "*** Will kill the old fswatch PID:${PID} ***"
    kill -9 $PID
    echo "undef" > /var/run/fswatch.pid
elif [ "$ExsitFsPid" != "" ]; then
    echo "*** Exsit fswatch PID:${ExsitFsPid}, kill it ***"
    kill -9 "$ExsitFsPid"
fi
(fswatch -tuxr /run/media & echo $! >&3) 3>/var/run/fswatch.pid | grep --line-buffered "Created\|Removed\|Updated\|Modified" | tee -a /var/log/fswatchlog.txt &
echo $! > /var/run/fswatch.pid

exit 0
